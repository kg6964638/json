//definir objeto//

const alumnos = [
    {
        matricula: '2',
        nombre: 'Jose Lopez',
        domicilio: 'Av. del Mar 12000',
        especialidad: 'Tec. Informatica',
        promedio: '8.9'
    },
    {
        matricula: '3',
        nombre: 'Maria Lopez',
        domicilio: 'Av. del Bosque Jose',
        especialidad: 'Tecnico en Informatica',
        promedio: '8.7',
    },
    {
        matricula: '4',
        nombre: 'Jose Perez',
        domicilio: 'Av. del Mar 1200',
        especialidad: 'Tecnico en Informatica',
        promedio: '9',
    },
    {
        matricula: '2023777',
        nombre: 'Jose Jose',
        domicilio: 'Av. de la Pradera 10201',
        especialidad: 'Tecnico en Informatica',
        promedio: '5'
    },
    {
        matricula: '2023789',
        nombre: 'Joanna',
        domicilio: 'Av. del Rio 3200',
        especialidad: 'Tecnico en Informatica',
        promedio: '90.0'
    }
];

document.getElementById('searchForm').addEventListener('submit', function(e) {
    e.preventDefault();
    const matricula = document.getElementById('txtMatricula').value.trim();
    const alumno = alumnos.find(al => al.matricula === matricula);
    const resultDiv = document.getElementById('result');
    const messageDiv = document.getElementById('message');

    if (alumno) {
        document.getElementById('txtNombre').textContent = alumno.nombre;
        document.getElementById('txtDomicilio').textContent = alumno.domicilio;
        document.getElementById('txtEspecialidad').textContent = alumno.especialidad;
        document.getElementById('txtPromedio').textContent = alumno.promedio;
        messageDiv.textContent = 'Alumno encontrado.';
        messageDiv.style.color = 'green';
    } else {
        document.getElementById('txtNombre').textContent = '';
        document.getElementById('txtDomicilio').textContent = '';
        document.getElementById('txtEspecialidad').textContent = '';
        document.getElementById('txtPromedio').textContent = '';
        messageDiv.textContent = 'Error: Matrícula no encontrada.';
        messageDiv.style.color = 'red';
    }
});
 
